<?php

namespace App\Http\Controllers;

use App\UserTest;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index()
    {
      $getData = UserTest::get();
      // dd($getData);
        return view('index',compact('getData'));
    }

    public function store(Request $request)
    {
        //LOGIC HERE
        
        $count = UserTest::count();        
        if($count % 2 == 0){
          UserTest::create([
                              'name' => $request->name,
                              'parity' => 'Odd',
                          ]);
        }
        else
        {
          UserTest::create([
                              'name' => $request->name,
                              'parity' => 'Even',
                          ]);
        }
        return redirect()->route('index');
    }

}
